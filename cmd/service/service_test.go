package main

import (
	"os"
	"syscall"
	"testing"
	"time"

	mocks "gitlab.com/unboundsoftware/apex-mocks"
)

func TestMainFunc_Success(t *testing.T) {
	os.Args = []string{"s3uploader", "--port", "7777", "--bucket", "test-bucket-somewhere", "--return-url", "https://example.org"}
	go func() {
		time.Sleep(time.Second)
		_ = syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
	}()
	main()
}

func TestMainFunc_Invalid_AWS_Config(t *testing.T) {
	os.Args = []string{"s3uploader", "--port", "7777", "--bucket", "test-bucket-somewhere", "--return-url", "https://example.org"}
	_ = os.Setenv("AWS_MAX_ATTEMPTS", "invalid")
	defer func() {
		_ = os.Unsetenv("AWS_MAX_ATTEMPTS")
	}()
	main()
}

func Test_start(t *testing.T) {
	type args struct {
		port   int
		bucket string
		url    string
	}
	tests := []struct {
		name       string
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "invalid port",
			args: args{
				port:   77777,
				bucket: "some-bucket",
				url:    "https://example.org",
			},
			wantErr: false,
			wantLogged: []string{
				"info: Serving HTTP API on :77777",
				"error: HTTP server failed",
				"info: Shutdown of HTTP server complete",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logger := mocks.New()
			CLI.Port = tt.args.port
			CLI.Bucket = tt.args.bucket
			CLI.ReturnURL = tt.args.url
			if err := start(logger); (err != nil) != tt.wantErr {
				t.Errorf("start() error = %v, wantErr %v", err, tt.wantErr)
			}
			logger.Check(t, tt.wantLogged)
		})
	}
}
